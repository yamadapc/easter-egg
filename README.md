easter-egg
==========
An easter egg for Toggl. It's a musical instrument (a piano - but maybe more
sounds are coming in the future), _integrated_ into the Toggl reports page.

I put "integrated" in italics, because currently it's just over a screenshot,
but doing actual integration of our code into the real Toggl application
wouldn't take that much more effort.

## 1. Starting out
We started out thinking about this project on April 15. Since we only had from
the 14th of April until the 20th, maybe this gave us a good early start. The
team just had a nice conversation on the Park next to our Hotel, while we sat
on the grass and looked at the beautiful Tel Aviv sunset, over the beach's
horizon (it was pretty nice, so I feel it's worth mentioning that).

After deciding that we didn't really want to do a game or anything that took
too much of our time together, we started thinking of ways to integrate it into
the Application. The obvious choice was the Homepage Timer, but it occurred to
us that the possibility of integration into that page was relatively
small. Going from the reports pages, maybe starting out from some chart seemed
like a better choice.

Some ideas flew by: a "flappy birds" clone flying between report bars, a 2D
platform style runner jumping around from one chart bar to another, etc.

We set out on doing a Piano, trying to make it in a way that would make both
musicians (who could play it) and non-musicians have fun with it. The idea then
became to write a little music tutorial before the piano was fully playable.

## 2. Getting the 1st samples
We scraped the first samples out of a project called _HTML5 Piano_. It was just
a matter of downloading the HTML on its webpage demo and using some Emacs/Vim
tricks with macros to extract the audio tags and get the samples on our own
page. After about 2-3 hours of work we had a functional JavaScript
keyboard. Andriin quickly wrote the start-up animation at the same time.

## 3. Transcribing Fur Elise
The next step was to build the tutorial. Liisa and me tried to read the sheet
music a little bit and figure how many octaves (roughly notes) we'd need. We
were missing some of them, but that was a problem for another time. So that I
could fix-up problems with the keyboard, Liisa set out to transcribe the sheet
music into the American notation (`[ABCDEFG]#?` if you're the specification
kind of person). Her work is available on this repository under the
[fur-elise-seed.txt](/fur-elise-seed.txt) file.

First, more Emacs/Vim tricks were used to convert the notes into a JSON array
of strings. I then wrote a [simple script](/fur-elise-gen.js) to parse the file
and convert it to the `Object`s that was chosen to represent notes. We now had
the song in, so I played around with trying to make the code reproduce the song
automatically, which didn't make it into the final product, but was used for
doing the introductory playing before the tutorial.

## 4. Usability and Design
Heidi had designed a nice keyboard layout and we got set on colors and
different states for the keys, so things were starting to look really nice.

Within another 1-2 hours we had a first version of the tutorial working. There
were usability issues, but we could fix them later on.

Adriin made a lot of visual tweaks and bug fixes in the two days that followed
that.

On our deadline day, I put into more work trying to replace the samples with
better ones. Because the range of piano samples we'd extracted from the _HTML5
Piano_ project wasn't enough. For this, I wrote [another script](/better-samples/download-script)
through VIM/jQuery hacking to download the better samples and [one last script](/better-samples/conversion-script)
to convert the samples into OGG and remove silences from the files. This used
the `ffmpeg` and `sox` command-line utilities.

## License
This code is licensed under the MIT license. Please refer to the [LICENSE
file](/LICENSE) for more information.
