'use strict'
var _ = require('lodash');
var furElise1 = require('./fur-elise-seed.json');
var result = _.map(furElise1, function(note) {
  if(_.contains(note, 1)|| _.contains(note, 2)) {
    var pitch = +note.charAt(note.length - 1) + 1
    if(pitch < 3) pitch += 1;
    else if(pitch > 4) pitch -= 1;
    return { pitch: pitch, note: note.slice(0, note.length - 1), };
  }
  return { pitch: 3, note: note, };
});
console.log(result);
