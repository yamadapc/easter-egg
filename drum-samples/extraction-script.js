'use strict';
var fs = require('fs');
var path = require('path');
var cheerio = require('cheerio');

var file = fs.readFileSync(path.join(process.cwd(), 'drum-samples.html'));
var $ = cheerio.load(file);
var links = $('a').map(function(i, el) {
  return 'wget -nc "http://sampleswap.org/' + $(el).attr('href') + '"';
}).get();
console.log(links.join('\n'));
