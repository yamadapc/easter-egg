'use strict'; /* global describe, it */
var should = require('should');
var bootstrap = require('..');

describe('easter-egg', function() {
  describe('bootstrap()', function() {
    it('gets exposed', function() {
      should.exist(bootstrap);
      bootstrap.should.be.instanceof(Function);
    });

    it('register event listeners to DOM elements', function() {
    });
  });
});
