'use strict';
var $ = window.jQuery = window.$ = require('jquery');
var _ = require('lodash');
require('../node_modules/jquery.easing/jquery.easing.1.3.js');
var furElise = require('./fur-elise.json');
var startAnimation = require('./start-animation');

exports = module.exports = bootstrap;

$(bootstrap);

var WHITE_KEY_TO_NOTES = {
  'key-0': {note: 'F', pitch: 2,},
  'key-1': {note: 'G', pitch: 2,},
  'key-2': {note: 'A', pitch: 2,},
  'key-3': {note: 'B', pitch: 2,},
  'key-4': {note: 'C', pitch: 3,},
  'key-5': {note: 'D', pitch: 3,},
  'key-6': {note: 'E', pitch: 3,},
  'key-7': {note: 'F', pitch: 3,},
  'key-8': {note: 'G', pitch: 3,},
  'key-9': {note: 'A', pitch: 3,},
  'key-10': {note: 'B', pitch: 3,},
  'key-11': {note: 'C', pitch: 4,},
  'key-12': {note: 'D', pitch: 4,},
  'key-13': {note: 'E', pitch: 4,},
  'key-14': {note: 'F', pitch: 4,},
};
var NOTES_TO_WHITE_KEYS = _.chain(WHITE_KEY_TO_NOTES)
  .mapValues(function(spec) {
    return spec.note + spec.pitch;
  })
  .invert().value();

var BLACK_KEY_TO_NOTES = {
  'key-0': {note: 'F#', pitch: 2,},
  'key-1': {note: 'G#', pitch: 2,},
  'key-2': {note: 'A#', pitch: 2,},
  'key-4': {note: 'C#', pitch: 3,},
  'key-5': {note: 'D#', pitch: 3,},
  'key-7': {note: 'F#', pitch: 3,},
  'key-8': {note: 'G#', pitch: 3,},
  'key-9': {note: 'A#', pitch: 3,},
  'key-11': {note: 'C#', pitch: 4,},
  'key-12': {note: 'D#', pitch: 4,},
  'key-14': {note: 'F#', pitch: 5,},
};
var NOTES_TO_BLACK_KEYS = _.chain(BLACK_KEY_TO_NOTES)
  .mapValues(function(spec) {
    return spec.note + spec.pitch;
  })
  .invert().value();

var KEYBOARD_KEYS_TO_NOTES = {
  'Q': {note: 'F', pitch: 2,},
  'W': {note: 'G', pitch: 2,},
  'E': {note: 'A', pitch: 2,},
  'R': {note: 'B', pitch: 2,},
  'T': {note: 'C', pitch: 3,},
  'Y': {note: 'D', pitch: 3,},
  'U': {note: 'E', pitch: 3,},
  'I': {note: 'F', pitch: 3,},
  'O': {note: 'G', pitch: 3,},
  'P': {note: 'A', pitch: 3,},
  'A': {note: 'B', pitch: 3,},
  'S': {note: 'C', pitch: 4,},
  'D': {note: 'D', pitch: 4,},
  'F': {note: 'E', pitch: 4,},
  'G': {note: 'F', pitch: 4,},
  '1': {note: 'F#', pitch: 2,},
  '2': {note: 'G#', pitch: 2,},
  '3': {note: 'A#', pitch: 2,},
  '4': {note: 'C#', pitch: 3,},
  '5': {note: 'D#', pitch: 3,},
  '6': {note: 'F#', pitch: 3,},
  '7': {note: 'G#', pitch: 3,},
  '8': {note: 'A#', pitch: 3,},
  '9': {note: 'C#', pitch: 4,},
  '0': {note: 'D#', pitch: 4,},
};

function idForNote(note, pitch) {
  note = note.toUpperCase();
  pitch || (pitch = 2);
  switch(note) {
    case 'A':
      return '#tone-' + pitch + 'A';
    case 'A#':
      return '#tone-' + pitch + 'As';
    case 'B':
      return '#tone-' + pitch + 'B';
    case 'C':
      return '#tone-' + pitch + 'C';
    case 'C#':
      return '#tone-' + pitch + 'Cs';
    case 'D':
      return '#tone-' + pitch + 'D';
    case 'D#':
      return '#tone-' + pitch + 'Ds';
    case 'E':
      return '#tone-' + pitch + 'E';
    case 'F':
      return '#tone-' + pitch + 'F';
    case 'F#':
      return '#tone-' + pitch + 'Fs';
    case 'G':
      return '#tone-' + pitch + 'G';
    case 'G#':
      return '#tone-' + pitch + 'Gs';
    default:
      throw new Error('I don\'t know this note');
  }
}

function silenceAll() {
  $('audio').map(function(i, el) {
    el.pause();
    el.currentTime = 0;
  });
}

var play$current;
function play(note, pitch, stopCurrentSound) {
  var id = idForNote(note, pitch);
  var el = $(id).get(0);
  var keyEl = specToKeyEl({ note: note, pitch: pitch, });
  if(stopCurrentSound) stopCurrent();
  if(el) {
    el.currentTime = 0;
    el.play();
  }
  keyEl.trigger('playNote');
  play$current = {
    el: el,
    keyEl: keyEl,
  };
}
function stopCurrent() {
  if(!play$current) return;
  play$current.el && play$current.el.pause();
  play$current.el && (play$current.el.currentTime = 0);
}

var fingerDrag = [
  {note: 'F', pitch: 2, timing: 58},
  {note: 'G', pitch: 2, timing: 57},
  {note: 'A', pitch: 2, timing: 55},
  {note: 'B', pitch: 2, timing: 54},
  {note: 'C', pitch: 3, timing: 52},
  {note: 'D', pitch: 3, timing: 49},
  {note: 'E', pitch: 3, timing: 46},
  {note: 'F', pitch: 3, timing: 42},
  {note: 'G', pitch: 3, timing: 40},
  {note: 'A', pitch: 3, timing: 39},
  {note: 'B', pitch: 3, timing: 38},
  {note: 'C', pitch: 4, timing: 37},
  {note: 'D', pitch: 4, timing: 36},
  {note: 'E', pitch: 4, timing: 35},
  {note: 'F', pitch: 4, timing: 33},
];

function playTimed(notes, cb) {
  var total = 0;

  for(var i = 0, len = notes.length; i < len; i++) {
    var note = notes[i];
    var keyEl = specToKeyEl(note);
    if(i === 0) {
      play(note.note, note.pitch, false);
      keyEl.addClass('active');
      setTimeout(function(keyEl) { keyEl.addClass('anim').removeClass('active'); }.bind(null, keyEl), 50);
      setTimeout(function(keyEl) { keyEl.removeClass('anim'); }.bind(null, keyEl), 400);
    } else {
      total += notes[i-1].timing;
      setTimeout(
        function(note, keyEl) {
          keyEl.addClass('active');
          setTimeout(function(keyEl) { keyEl.addClass('anim').removeClass('active'); }.bind(null, keyEl), 50);
          setTimeout(function(keyEl) { keyEl.removeClass('anim'); }.bind(null, keyEl), 400);
          play(note.note, note.pitch, false);
        }.bind(null, note, keyEl),
        total
      );
    }
  }
  setTimeout(stopCurrent, total);
  setTimeout(cb, total);
}

function keyElToSpec(isBlack, el) {
  var key = _.find(el.className.split(' '), function(name) {
    return name.indexOf('key-') === 0;
  });
  var obj;
  if(isBlack) {
    obj = BLACK_KEY_TO_NOTES[key];
  } else obj = WHITE_KEY_TO_NOTES[key];
  return obj;
}

function specToKeyEl(obj) {
  var selector;
  if(obj.note.indexOf('#') !== -1) {
    selector = '.black-key.' + NOTES_TO_BLACK_KEYS[obj.note + obj.pitch];
  } else {
    selector = '.white-key.' + NOTES_TO_WHITE_KEYS[obj.note + obj.pitch];
  }
  return $(selector);
}

function startFurEliseTutorial() {
  var len = furElise.length;
  loop(0);
  function loop(i) {
    var obj = furElise[i];
    prepareToHoverPlay(obj.note, obj.pitch, function() {
      if(i < len - 1) loop(i + 1);
      else {
        setTimeout(endFurEliseTutorial, 2000);
      }
    });
  }
}

function endFurEliseTutorial() {
  playTimed(fingerDrag, function(err) {
    if(err) throw err;
    if(prepareToHoverPlay$current) {
      prepareToHoverPlay$current.off('mouseover.tutorial');
      prepareToHoverPlay$current.removeClass('tutorial-key');
    }

    $('.skip').hide();
  });
}

var prepareToHoverPlay$current;
function prepareToHoverPlay(note, pitch, cb) {
  var $el = specToKeyEl({ note: note, pitch: pitch, });

  if(prepareToHoverPlay$current) {
    prepareToHoverPlay$current.off('mouseover.tutorial');
  }
  prepareToHoverPlay$current = $el;

  $el.addClass('tutorial-key');
  $el.one('playNote', function() {
    $el.off('mouseover.tutorial');
    $el.removeClass('tutorial-key');
    cb();
  });
  $el.on('mouseover.tutorial', function() {
    play(note, pitch);
  });
}

var keysDown = [];

function bootstrap() {
  $('#input_description').keyup(function() {
    $('.btn-filter').css({opacity: 1});
  });
  $('.btn-filter').click(function() {
    if($('#input_description').val().toLowerCase() == 'rockstar!') {
      startEgg();
      $('.btn-filter').off('click');
    }
  });
  $('form').submit(function(e) {
    e.preventDefault();
    $('#input_description').blur();
    $('.btn-filter').css({opacity: 0});
  });
}

function startEgg() {
  $('body').on('keydown', onKeydown);

  $('body').on('keyup', onKeyup);

  var $skip = $('.skip');
  $skip.on('click', function() {
    endFurEliseTutorial();
  });

  startAnimation.run(function(err) {
    if(err) throw err;
    attachEvents();
    playTimed(fingerDrag, function(err) {
      if(err) throw err;
      setTimeout(function() {
        startFurEliseTutorial();
        $skip.show();
      }, 1000);
    });
  });

  function attachEvents() {
    var $whiteKeys = $('.white-key');
    var $blackKeys = $('.black-key');
    $whiteKeys.each(function(_i, el) {
      attachEvent(false, el);
    });
    $blackKeys.each(function(_i, el) {
      attachEvent(true, el);
    });
  }

  var mousedown = false;

  function attachEvent(isBlack, el) {
    var obj = keyElToSpec(isBlack, el);
    if(!obj) return;
    var keyEl = specToKeyEl(obj);

    function onPress(/*evt*/) {
      keyEl.addClass('active');
      play(obj.note, obj.pitch);
    }

    function onRelease(/*evt*/) {
      keyEl.removeClass('active');
      mousedown = false;
    }

    $(el).on('mouseenter', function(/*evt*/) {
      if(mousedown) {
        onPress();
        $(document).one('mouseup.dragover', onRelease);
      }
    });

    $(el).on('mouseleave', function(/*evt*/) {
      if(mousedown) {
        keyEl.removeClass('active');
        $(document).off('mouseup.dragover');
      }
    });

    $(el).on('mousedown', function(evt) {
      evt.preventDefault();
      evt.stopPropagation();
      mousedown = true;
      onPress();
      $(document).one('mouseup', onRelease);
    });

  }
}

function onKeyup(e) {
  delete keysDown[keysDown.indexOf(e.keyCode)];
  var char = String.fromCharCode(e.keyCode);
  var obj = KEYBOARD_KEYS_TO_NOTES[char];
  if(obj) {
    var keyEl = specToKeyEl(obj);
    keyEl.removeClass('active');
  }
}

function onKeydown(e) {
  if(e.keyCode === 32) return silenceAll();
  if(e.keyCode === 13) return startLooping();
  if(keysDown.indexOf(e.keyCode) !== -1) return;
  keysDown.push(e.keyCode);
  var char = String.fromCharCode(e.keyCode);
  var obj = KEYBOARD_KEYS_TO_NOTES[char];
  if(obj) {
    var keyEl = specToKeyEl(obj);
    keyEl.addClass('active');
    play(obj.note, obj.pitch);
  }
}

var startLooping$interval;
function startLooping() {
  var loop = [];
  var down = {};
  var startTime = new Date().getTime();

  $('body').on('keydown', function(e) {
    if(down[e.keyCode]) return;
    down[e.keyCode] = true;
    var diff = e.timeStamp - startTime;
    loop.push({ event: e, diff: diff });
  });

  $('body').on('keyup', function(e) {
    keysDown[e.keyCode] = false;
  });

  if(startLooping$interval) clearInterval(startLooping$interval);
  startLooping$interval = setInterval(function() {
    _.each(loop, function(e) {
      setTimeout(function() {
        onKeydown({keyCode: e.event.keyCode});
        onKeyup({keyCode: e.event.keyCode,});
      }, e.diff);
    });
    startTime = new Date().getTime();
  }, 4000);
}
