var $ = require('jquery');

module.exports = {
  run: function (cb) {

    // Labels

    $('.bar .label').each(function (i, el) {
      setTimeout(function() {
        $(el).css({position: 'relative'}).animate({top: '-80px', opacity: 0}, 250, function() {
          $(el).remove();
        }) }
      , i * 80);
    });

    // Bars to the top

    setTimeout(function() {
      var disappearing = [3,6,10,13];
      $('.bar .fill').each(function (i, el) {
        setTimeout(function() {
          var pos = $(el).position();
          $(el).addClass('black-key key-' + i);
          if(disappearing.indexOf(i) > -1) {
            $(el).css({position: 'absolute', top: pos.top}).animate({top: '-100%'}, 1350, 'easeOutExpo');
          } else {
            $(el).css({position: 'absolute', top: pos.top}).animate({top: '0px', height: '75%'}, 750, 'easeOutBounce');
          }
        }, i * 130);
      });
    }, 700);

    // White keys appear

    setTimeout(function() {
      $('.white-key').each(function (i, el) {
        setTimeout(function() {
          $(el).animate({opacity: 1}, 300);
        }, i * 130);
      });
    }, 700);

    // Billables disappear

    setTimeout(function() {
      $('.bar .fill .billable').each(function (i, el) {
        setTimeout(function() {
          var pos = $(el).position();
          $(el).slideUp(150);
        }, i * 130);
      });
    }, 700);

    // Grid disappears

    setTimeout(function() {
      $('.horizontal-grid .line').each(function (i, el) {
        setTimeout(function() {
          $(el).fadeOut();
        }, i * 70);
      });
    }, 1700);

    // Grid disappears

    setTimeout(function() {
      $('.chart-axis-y .label').each(function (i, el) {
        setTimeout(function() {
          $(el).fadeOut();
        }, i * 70);
      });
    }, 1900);

    setTimeout(function() {
      cb(null);
    }, 3000);
  }
}
